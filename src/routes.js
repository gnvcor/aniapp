export default {

    home: {
        initialRoute: true,

        title: 'Главная',
        component: require('./scenes/Home').default,

        children: {
            dogs: {
                title: 'Собаки',
                component: require('./scenes/Dogs').default,

                children: {
                    detail: {
                        title: 'Детальная страница собак',
                        component: require('./scenes/Detail').default
                    }
                }
            },

            cats: {
                title: 'Кошки',
                component: require('./scenes/Cats').default,

                children: {
                    detail: {
                        title: 'Детальная страница кошек',
                        component: require('./scenes/Detail').default
                    }
                }
            },

            rodents: {
                title: 'Грызуны',
                component: require('./scenes/Rodents').default,

                children: {
                    detail: {
                        title: 'Детальная страница грызунов',
                        component: require('./scenes/Detail').default
                    }
                }
            },

            fish: {
                title: 'Рыбки',
                component: require('./scenes/Fish').default,

                children: {
                    detail: {
                        title: 'Детальная страница рыбок',
                        component: require('./scenes/Detail').default
                    }
                }
            },

            birds: {
                title: 'Птицы',
                component: require('./scenes/Birds').default,

                children: {
                    detail: {
                        title: 'Детальная страница птиц',
                        component: require('./scenes/Detail').default
                    }
                }
            },

            'exotic-animals': {
                title: 'Экзотические животные',
                component: require('./scenes/ExoticAnimals').default,

                children: {
                    detail: {
                        title: 'Детальная страница экзотических животных',
                        component: require('./scenes/Detail').default
                    }
                }
            }
        }
    },

    themes: {
        title: 'Изменить тему',
        component: require('./scenes/Themes').default
    },

    about: {
        title: 'О приложении',
        component: require('./scenes/About').default
    },

    personal: {
        title: 'Мои животные',
        component: require('./scenes/personal/Personal').default
    },

    auth: {
        title: 'Авторизация',
        component: require('./scenes/auth/Auth').default,

        children: {
            register: {
                title: 'Регистрация',
                component: require('./scenes/auth/Register').default
            }
        }
    }
}