import { AsyncStorage } from 'react-native';
//import SplashScreen from '@remobile/react-native-splashscreen';
import alt from '../alt';
import AppActions from '../actions/AppActions';
import AppRoutes from '../actions/AppRoutes';
import AppUser from '../actions/AppUser';

const THEME = '@Storage:theme';
const ROUTE = '@Storage:route';
const USER = '@Storage:user';

class AppStore {

    constructor() {
        this._loadTheme();
        this._loadRoute();
        this._loadUser();

        this.bindListeners({
            handleUpdateTheme: AppActions.UPDATE_THEME,
            handleUpdateRoute: AppRoutes.UPDATE_ROUTES,
            handleUpdateUser: AppUser.UPDATE_USER
        });
    }

    _loadUser = () => {
        AsyncStorage.getItem(USER).then((user_id) => {
            this.user = user_id || null;
            AppUser.updateUser(this.user);
        });
    };

    _loadTheme = () => {
        AsyncStorage.getItem(THEME).then((value) => {
            this.theme = value || 'paperTeal';
            AppActions.updateTheme(this.theme);
            //SplashScreen.hide();
        });
    };

    _loadRoute = () => {
        AsyncStorage.getItem(ROUTE).then((value) => {
            this.route = value || 'home';
            AppRoutes.updateRoutes(this.route);
        });
    };

    handleUpdateUser(id) {
        this.user = id;
        AsyncStorage.setItem(USER, id);
    }

    handleUpdateTheme(name) {
        this.theme = name;
        AsyncStorage.setItem(THEME, name);
    }

    handleUpdateRoute(value) {
        this.route = value;
        AsyncStorage.setItem(ROUTE, value);
    }

}

export default alt.createStore(AppStore, 'AppStore');
