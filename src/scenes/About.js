import React, { Component } from 'react';
import {
    View,
    Text,
    IntentAndroid,
    ScrollView
} from 'react-native';

import { Button, COLOR } from 'react-native-material-design';
import AppStore from '../stores/AppStore';

export default class About extends Component {

    render() {
        const theme = AppStore.getState().theme;

        return (
            <ScrollView style={{ padding: 16 }}>
                <Text style={{ marginBottom: 10 }}>Версия 1.1</Text>
                <Text style={{ marginBottom: 10 }}>Email: nikita.kolibri@gmail.com</Text>
                <Text style={{ marginBottom: 10 }}>AniApp © 2016</Text>
                <Text style={{ marginBottom: 10 }}>Источники:</Text>
                <View style={{ paddingBottom: 16 }}>
                    <Button primary={theme} text="ru.wikipedia.org" onPress={() => IntentAndroid.openURL('https://ru.wikipedia.org/')} />
                    <Button primary={theme} text="dogcentr.ru" onPress={() => IntentAndroid.openURL('http://dogcentr.ru/')} />
                    <Button primary={theme} text="club.xdogs.ru" onPress={() => IntentAndroid.openURL('http://club.xdogs.ru/')} />
                    <Button primary={theme} text="petsik.ru" onPress={() => IntentAndroid.openURL('http://petsik.ru/')} />
                    <Button primary={theme} text="dogsecrets.ru" onPress={() => IntentAndroid.openURL('http://dogsecrets.ru/')} />
                    <Button primary={theme} text="all-small-dogs.ru" onPress={() => IntentAndroid.openURL('http://all-small-dogs.ru/')} />
                    <Button primary={theme} text="blog.thezoo.ru" onPress={() => IntentAndroid.openURL('http://blog.thezoo.ru/')} />
                    <Button primary={theme} text="dressirovka-sobak.com" onPress={() => IntentAndroid.openURL('http://dressirovka-sobak.com/')} />
                    <Button primary={theme} text="v-mire-sobak.ru" onPress={() => IntentAndroid.openURL('http://v-mire-sobak.ru/')} />
                    <Button primary={theme} text="i-fakt.ru" onPress={() => IntentAndroid.openURL('http://i-fakt.ru/')} />
                    <Button primary={theme} text="huskypuppies.ru" onPress={() => IntentAndroid.openURL('http://huskypuppies.ru/')} />
                    <Button primary={theme} text="pekingese555.jimdo.com" onPress={() => IntentAndroid.openURL('http://pekingese555.jimdo.com/')} />
                    <Button primary={theme} text="pmysobaka.ru" onPress={() => IntentAndroid.openURL('http://mysobaka.ru/')} />
                    <Button primary={theme} text="animalsik.com" onPress={() => IntentAndroid.openURL('http://animalsik.com/')} />
                    <Button primary={theme} text="фактъ.рф" onPress={() => IntentAndroid.openURL('http://xn--80auyf7a.xn--p1ai/')} />
                    <Button primary={theme} text="vetsystem.ru" onPress={() => IntentAndroid.openURL('http://vetsystem.ru/')} />
                    <Button primary={theme} text="poisk-druga.ru" onPress={() => IntentAndroid.openURL('http://poisk-druga.ru/')} />
                    <Button primary={theme} text="tvaryny.com" onPress={() => IntentAndroid.openURL('http://tvaryny.com/')} />
                    <Button primary={theme} text="poroda-sobak.com" onPress={() => IntentAndroid.openURL('http://poroda-sobak.com/')} />
                    <Button primary={theme} text="blog.vetatlas.ru" onPress={() => IntentAndroid.openURL('http://blog.vetatlas.ru/')} />
                    <Button primary={theme} text="glorypets.ru" onPress={() => IntentAndroid.openURL('http://glorypets.ru/')} />
                    <Button primary={theme} text="вашехобби.рф" onPress={() => IntentAndroid.openURL('http://www.xn--80acabqu3b5cza.xn--p1ai/')} />
                    <Button primary={theme} text="catgallery.ru" onPress={() => IntentAndroid.openURL('http://catgallery.ru/')} />
                    <Button primary={theme} text="pitomec.ru" onPress={() => IntentAndroid.openURL('http://www.pitomec.ru/')} />
                    <Button primary={theme} text="vsookoshkax.ru" onPress={() => IntentAndroid.openURL('http://vsookoshkax.ru/')} />
                    <Button primary={theme} text="zooatlas.ru" onPress={() => IntentAndroid.openURL('http://zooatlas.ru/')} />
                    <Button primary={theme} text="murlo.org" onPress={() => IntentAndroid.openURL('http://murlo.org/')} />
                    <Button primary={theme} text="zverenki.com" onPress={() => IntentAndroid.openURL('http://zverenki.com/')} />
                    <Button primary={theme} text="kupikota.ru" onPress={() => IntentAndroid.openURL('http://kupikota.ru/')} />
                    <Button primary={theme} text="cat.mau.ru" onPress={() => IntentAndroid.openURL('http://cat.mau.ru/')} />
                    <Button primary={theme} text="cat-world.ru" onPress={() => IntentAndroid.openURL('http://cat-world.ru/')} />
                    <Button primary={theme} text="kotenok.ru" onPress={() => IntentAndroid.openURL('http://www.kotenok.ru/')} />
                    <Button primary={theme} text="миркошек.рф" onPress={() => IntentAndroid.openURL('http://xn--e1afgbgom0e.xn--p1ai/')} />
                    <Button primary={theme} text="zooclub.ru" onPress={() => IntentAndroid.openURL('http://www.zooclub.ru/')} />
                    <Button primary={theme} text="hamsterhappy.ru" onPress={() => IntentAndroid.openURL('http://hamsterhappy.ru/')} />
                    <Button primary={theme} text="n-l-d.ru" onPress={() => IntentAndroid.openURL('http://n-l-d.ru/')} />
                    <Button primary={theme} text="pitomecdoma.ru" onPress={() => IntentAndroid.openURL('http://pitomecdoma.ru/')} />
                    <Button primary={theme} text="vdbr.ru" onPress={() => IntentAndroid.openURL('http://vdbr.ru/')} />
                    <Button primary={theme} text="kotovasia.net" onPress={() => IntentAndroid.openURL('http://kotovasia.net/')} />
                    <Button primary={theme} text="шиншиллка.рф" onPress={() => IntentAndroid.openURL('http://xn--80apaieal0gc.xn--p1ai/')} />
                    <Button primary={theme} text="chinchillas.by" onPress={() => IntentAndroid.openURL('http://chinchillas.by/')} />
                    <Button primary={theme} text="anfiska.biz" onPress={() => IntentAndroid.openURL('http://www.anfiska.biz/')} />
                    <Button primary={theme} text="vao-priut.org" onPress={() => IntentAndroid.openURL('http://vao-priut.org/')} />
                    <Button primary={theme} text="rostki.info" onPress={() => IntentAndroid.openURL('http://www.rostki.info/')} />
                    <Button primary={theme} text="zveryatam.ru" onPress={() => IntentAndroid.openURL('http://zveryatam.ru/')} />
                    <Button primary={theme} text="lubim4iki.ru" onPress={() => IntentAndroid.openURL('http://lubim4iki.ru/')} />
                    <Button primary={theme} text="vse-kroliki.ru" onPress={() => IntentAndroid.openURL('http://vse-kroliki.ru/')} />
                    <Button primary={theme} text="msvinka.ru" onPress={() => IntentAndroid.openURL('http://www.msvinka.ru/')} />
                    <Button primary={theme} text="fermagid.ru" onPress={() => IntentAndroid.openURL('http://fermagid.ru/')} />
                    <Button primary={theme} text="decorativniecrolici.blogspot.ru" onPress={() => IntentAndroid.openURL('http://decorativniecrolici.blogspot.ru/')} />
                    <Button primary={theme} text="djiv.ru" onPress={() => IntentAndroid.openURL('http://djiv.ru/')} />
                    <Button primary={theme} text="8lap.ru" onPress={() => IntentAndroid.openURL('http://www.8lap.ru/')} />
                    <Button primary={theme} text="zooclub.by" onPress={() => IntentAndroid.openURL('http://zooclub.by/')} />
                    <Button primary={theme} text="luxbeauty.ru" onPress={() => IntentAndroid.openURL('http://www.luxbeauty.ru/')} />
                    <Button primary={theme} text="domveterinar.ru" onPress={() => IntentAndroid.openURL('http://domveterinar.ru/')} />
                    <Button primary={theme} text="awesomeworld.ru" onPress={() => IntentAndroid.openURL('http://awesomeworld.ru/')} />
                    <Button primary={theme} text="theanimalworld.ru" onPress={() => IntentAndroid.openURL('http://www.theanimalworld.ru/')} />
                    <Button primary={theme} text="aquariumguide.ru" onPress={() => IntentAndroid.openURL('http://aquariumguide.ru/')} />
                    <Button primary={theme} text="vekzotike.ru" onPress={() => IntentAndroid.openURL('http://vekzotike.ru/')} />
                    <Button primary={theme} text="aquarists.ru" onPress={() => IntentAndroid.openURL('http://aquarists.ru/')} />
                    <Button primary={theme} text="popugaipark.ru" onPress={() => IntentAndroid.openURL('http://popugaipark.ru/')} />
                    <Button primary={theme} text="vpapagayo.blogspot.ru" onPress={() => IntentAndroid.openURL('http://vpapagayo.blogspot.ru/')} />
                    <Button primary={theme} text="vmirefactov.ru" onPress={() => IntentAndroid.openURL('http://vmirefactov.ru/')} />
                    <Button primary={theme} text="4parrots.ru" onPress={() => IntentAndroid.openURL('http://4parrots.ru/')} />
                    <Button primary={theme} text="zyblik.info" onPress={() => IntentAndroid.openURL('http://www.zyblik.info/')} />
                    <Button primary={theme} text="birdsearth.ru" onPress={() => IntentAndroid.openURL('http://birdsearth.ru/')} />
                    <Button primary={theme} text="happy-giraffe.ru" onPress={() => IntentAndroid.openURL('http://www.happy-giraffe.ru/')} />
                    <Button primary={theme} text="veterinarka.ru" onPress={() => IntentAndroid.openURL('http://www.veterinarka.ru/')} />
                    <Button primary={theme} text="mybirds.ru" onPress={() => IntentAndroid.openURL('http://www.mybirds.ru/')} />
                    <Button primary={theme} text="moikesha.ru" onPress={() => IntentAndroid.openURL('http://moikesha.ru/')} />
                    <Button primary={theme} text="modli.ru" onPress={() => IntentAndroid.openURL('http://modli.ru/')} />
                    <Button primary={theme} text="kotopes.ru" onPress={() => IntentAndroid.openURL('http://kotopes.ru/')} />
                    <Button primary={theme} text="vet.apreka.ru" onPress={() => IntentAndroid.openURL('http://vet.apreka.ru/')} />
                    <Button primary={theme} text="nashvet.ru" onPress={() => IntentAndroid.openURL('http://nashvet.ru/')} />
                </View>
            </ScrollView>
        );
    }
}
