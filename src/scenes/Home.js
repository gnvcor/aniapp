import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Dimensions,
    PixelRatio,
    TouchableHighlight
} from 'react-native';

import Parallax from 'react-native-parallax';

var IMAGE_WIDTH = Dimensions.get('window').width;
var IMAGE_HEIGHT = IMAGE_WIDTH / 2;
var PIXEL_RATIO = PixelRatio.get();
var PARALLAX_FACTOR = 0.3;

import AppRoutes from '../actions/AppRoutes';

export default class Home extends Component {

    static contextTypes = {
        navigator: PropTypes.object.isRequired
    };

    changeRoute = (route) => {
        AppRoutes.updateRoutes(route);
    };

    render() {
        const { navigator } = this.context;

        return (
            <Parallax.ScrollView style={styles.scrollView}>
                <Parallax.Image
                    key={0}
                    style={styles.image}
                    overlayStyle={styles.overlay}
                    source={{ uri: 'http://nikitayt.bget.ru/menuImages/dog.jpg' }}
                    parallaxFactor={PARALLAX_FACTOR}
                    onPress={() => { navigator.forward("dogs");  this.changeRoute("home.dogs"); }}
                >
                    <Text style={styles.title}>
                        Собаки
                    </Text>
                </Parallax.Image>
                <Parallax.Image
                    key={1}
                    style={styles.image}
                    overlayStyle={styles.overlay}
                    source={{ uri: 'http://nikitayt.bget.ru/menuImages/cat.jpg' }}
                    parallaxFactor={PARALLAX_FACTOR}
                    onPress={() => { navigator.forward("cats"); this.changeRoute("home.cats"); }}
                >
                    <Text style={styles.title}>
                        Кошки
                    </Text>
                </Parallax.Image>
                <Parallax.Image
                    key={2}
                    style={styles.image}
                    overlayStyle={styles.overlay}
                    source={{ uri: 'http://nikitayt.bget.ru/menuImages/rabbit.jpg' }}
                    parallaxFactor={PARALLAX_FACTOR}
                    onPress={() => { navigator.forward("rodents"); this.changeRoute("home.rodents"); }}
                >
                    <Text style={styles.title}>
                        Грызуны
                    </Text>
                </Parallax.Image>
                <Parallax.Image
                    key={3}
                    style={styles.image}
                    overlayStyle={styles.overlay}
                    source={{ uri: 'http://nikitayt.bget.ru/menuImages/fish.jpg' }}
                    parallaxFactor={PARALLAX_FACTOR}
                    onPress={() => { navigator.forward("fish"); this.changeRoute("home.fish"); }}
                >
                    <Text style={styles.title}>
                        Рыбки
                    </Text>
                </Parallax.Image>
                <Parallax.Image
                    key={4}
                    style={styles.image}
                    overlayStyle={styles.overlay}
                    source={{ uri: 'http://nikitayt.bget.ru/menuImages/birds.jpg' }}
                    parallaxFactor={PARALLAX_FACTOR}
                    onPress={() => { navigator.forward("birds"); this.changeRoute("home.birds"); }}
                >
                    <Text style={styles.title}>
                        Птицы
                    </Text>
                </Parallax.Image>
                {/*
                    <Parallax.Image
                        key={5}
                        style={styles.image}
                        overlayStyle={styles.overlay}
                        source={{ uri: 'http://nikitayt.bget.ru/menuImages/exotic2.jpg' }}
                        parallaxFactor={PARALLAX_FACTOR}
                        onPress={() => { navigator.forward("exotic-animals"); this.changeRoute("home.exotic-animals"); }}
                    >
                        <Text style={styles.title}>
                            Экзотические животные
                        </Text>
                    </Parallax.Image>
                */}
            </Parallax.ScrollView>
        );
    }

}

var styles = StyleSheet.create({
    image: {
        height: IMAGE_HEIGHT,
    },
    overlay: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.3)',
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        lineHeight: 25,
        fontWeight: 'bold',
        color: 'white',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 1,
        shadowColor: 'black',
        shadowOpacity: 0.8,
    },
    url: {
        opacity: 0.5,
        fontSize: 10,
        position: 'absolute',
        color: 'white',
        left: 5,
        bottom: 5,
    }
});