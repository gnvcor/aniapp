import React, { Component } from 'react';
import {
    TouchableHighlight,
    View,
    Text
} from 'react-native';
import { Subheader, COLOR, PRIMARY_COLORS } from 'react-native-material-design';

import AppActions from '../actions/AppActions';
import AppRoutes from '../actions/AppRoutes';

export default class Themes extends Component {

    changeTheme = (theme) => {
        AppActions.updateTheme(theme);
    };

    changeRoute = (route) => {
        AppRoutes.updateRoutes(route);
    };

    render() {
        return (
            <View>
                <Subheader text="Выбор темы"/>
                <View style={styles.container}>
                    {PRIMARY_COLORS.map((color) => {
                        return (
                            <TouchableHighlight style={styles.test} key={color} onPress={() => { this.changeTheme(color); this.changeRoute("themes"); }}>
                                <View style={[styles.item, { backgroundColor: COLOR[`${color}500`].color }]}></View>
                            </TouchableHighlight>
                        );
                    })}
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    test: {
        width: 40,
        height: 40,
        marginBottom: 16,
        marginLeft: 16,
        borderRadius: 40
    },
    item: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 40
    }
};