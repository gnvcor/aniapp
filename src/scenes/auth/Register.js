import React, { Component, PropTypes } from 'react';
import {
    View,
    Text,
    IntentAndroid,
    ScrollView,
    TextInput,
    AsyncStorage,
    Alert
} from 'react-native';

import { Button, COLOR } from 'react-native-material-design';
import AppStore from '../../stores/AppStore';
import AppRoutes from '../../actions/AppRoutes';
import AppUser from '../../actions/AppUser';

import Firebase from 'firebase';

export default class Register extends Component {

    constructor(props){
        super(props);

        this.state = {
            email: '',
            password: ''
        };
    }

    static contextTypes = {
        navigator: PropTypes.object.isRequired
    };

    changeRoute = (route) => {
        AppRoutes.updateRoutes(route);
    };

    register(){
        const { navigator } = this.context;
        var self = this;

        Firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then(function(result) {
                AppUser.updateUser(result.uid);
                navigator.to("personal");
            }).catch(function(error) {
                if(error.code){
                    self.changeRoute("auth");

                    switch(error.code){
                        case "auth/weak-password":
                            Alert.alert("Ошибка!", "Слишком короткий пароль!");
                            break;

                        case "auth/email-already-in-use":
                            Alert.alert("Ошибка!", "Такой e-mail уже используется!");
                            break;

                        case "auth/invalid-email":
                            Alert.alert("Ошибка!", "Введен некорректный e-mail!");
                            break;

                        default:
                            Alert.alert("Ошибка!", "Ошибка создания пользователя!");
                    }

                }
        });
    }

    render() {
        const theme = AppStore.getState().theme;

        return (
            <ScrollView>
                <View style={styles.cont}>
                    <TextInput
                        placeholder={"Электронная почта"}
                        onChangeText={(text) => this.setState({email: text})}
                        value={this.state.email}
                    />
                    <TextInput
                        placeholder={"Пароль"}
                        onChangeText={(text) => this.setState({password: text})}
                        value={this.state.password}
                        secureTextEntry={true}
                    />
                </View>
                <View style={styles.contButton}>
                    <Button
                        primary={theme}
                        style={styles.button}
                        value="Зарегистрироваться"
                        raised={true}
                        onPress={()=> {this.register(); this.changeRoute("personal");}}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = {
    cont: {
        paddingTop: 5,
        paddingLeft: 15,
        paddingRight: 15
    },
    contButton: {
        paddingTop: 5,
        paddingLeft: 13,
        paddingRight: 13
    }
};
