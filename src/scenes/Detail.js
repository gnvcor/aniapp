import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView
} from 'react-native';

import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import DetailInfo from './tabDetail/DetailInfo';
import DetailNature from './tabDetail/DetailNature';
import DetailCare from './tabDetail/DetailCare';
import DetailDisease from './tabDetail/DetailDisease';
import DetailFacts from './tabDetail/DetailFacts';

import AppStore from '../stores/AppStore';

import { COLOR } from 'react-native-material-design';

export default class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            theme: AppStore.getState().theme
        }
    }

    componentDidMount = () => {
        AppStore.listen(this.handleAppStore);
    };

    componentWillUnmount() {
        AppStore.unlisten(this.handleAppStore);
    };

    handleAppStore = (store) => {
        this.setState({
            theme: store.theme
        });
    };

    render() {
        const { theme } = this.state;

        return (
            <ScrollableTabView
                initialPage={0}
                tabBarActiveTextColor={theme == undefined ? "grey" : COLOR[`${theme}500`].color}
                tabBarUnderlineColor={theme == undefined ? "grey" : COLOR[`${theme}500`].color}
                renderTabBar={() => <ScrollableTabBar />}>

                <DetailInfo data={this.props} tabLabel='Общая информация' />
                {this.props.category == "Рыбки" ? <DetailNature data={this.props} tabLabel='Поведение' /> : <DetailNature data={this.props} tabLabel='Характер' />}
                <DetailCare data={this.props} tabLabel='Уход' />
                {this.props.diseaseText != null ? <DetailDisease data={this.props} tabLabel='Болезни' /> : null}
                <DetailFacts data={this.props} tabLabel='Факты' />
            </ScrollableTabView>
        )
    }
}
