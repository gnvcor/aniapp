import React, { Component, PropTypes } from 'react';
import {
    ScrollView,
    TouchableNativeFeedback,
    View,
    Image,
    Text,
    Dimensions
} from 'react-native';

import { Avatar, Subheader, COLOR, Card, Icon, Button } from 'react-native-material-design';

import AppRoutes from '../../actions/AppRoutes';
import AppStore from '../../stores/AppStore';

import Firebase from 'firebase';

import GiftedSpinner from 'react-native-gifted-spinner';

import Swipeout from '../../../node_modules/rc-swipeout/lib/index.js';

export default class Personal extends Component {

    constructor() {
        super();
        this.state = {
            user: AppStore.getState().user,
            dataSource: [],
            loaded: true
        };

        this.itemsRef = this.getRef().child(this.state.user).child("animal");
    }

    static contextTypes = {
        navigator: PropTypes.object.isRequired
    };

    changeRoute = (route) => {
        AppRoutes.updateRoutes(route);
    };

    getRef() {
        return Firebase.database().ref();
    }

    listenForItems(itemsRef) {
        itemsRef.on('value', (snap) => {

            // get children as an array
            var items = [];
            snap.forEach((child) => {
                console.log(child.key);

                items.push({
                    name: child.val().name,
                    pic: child.val().uri,
                    category: child.val().category,
                    key: child.key,
                    numberParams: {
                        pound: child.val().numberParams.pound,
                        height: child.val().numberParams.height,
                        age: child.val().numberParams.age
                    },
                    infoText: child.val().infoText,
                    natureText: child.val().natureText,
                    factText: child.val().factText,
                    diseaseText: child.val().diseaseText,
                    careText: child.val().careText
                });
            });

            this.setState({
                dataSource: items,
                loaded: false
            });

        });
    }

    _removeItem(key) {
        this.itemsRef.child(key).remove();
    };

    componentDidMount() {
        this.listenForItems(this.itemsRef);
    }

    static contextTypes = {
        navigator: PropTypes.object.isRequired
    };

    render() {
        const theme = AppStore.getState().theme;
        const { navigator } = this.context;

        if (this.state.loaded == true) {
            var content = <View style={[{height: Dimensions.get('window').height - 81}, styles.spinnerContent]}><GiftedSpinner size="large" /></View>;
        } else {
            if (this.state.dataSource.length == 0) {
                var content = <View style={[{height: Dimensions.get('window').height - 81}, styles.spinnerContent]}>
                    <View>
                        <Text>Вы еще не добавили ни одного питомца!</Text>
                        <Button primary={theme} text="Начать!" onPress={() => {
                            navigator.to("home");
                            this.changeRoute("home");
                        }} />
                    </View>
                </View>;
            } else {
                var content = <View>
                    {this.state.dataSource.map((item, i) => (
                        <Swipeout
                            key={i}
                            style={{backgroundColor: '#eee'}}
                            autoClose="true"
                            right={[
                                {
                                  text: 'Удалить',
                                  onPress:() => {this._removeItem(item.key)},
                                  style: { backgroundColor: '#eee', color: '#1f1f1f', fontWeight: 'bold', marginRight: 7 }
                                }
                              ]}>
                            <Card style={i == 0 ? styles.contFirst : styles.cont}>
                                <TouchableNativeFeedback
                                    background={TouchableNativeFeedback.Ripple("rgba(153,153,153,.4)")}
                                    onPress={() => {
                                        switch (item.category) {
                                            case 'Собаки':
                                                navigator.to("home.dogs.detail", item.name, item);
                                                this.changeRoute("home.dogs");
                                                break;

                                            case 'Кошки':
                                                navigator.to("home.cats.detail", item.name, item);
                                                this.changeRoute("home.cats");
                                                break;

                                            case 'Грызуны':
                                                navigator.to("home.rodents.detail", item.name, item);
                                                this.changeRoute("home.rodents");
                                                break;

                                            case 'Рыбки':
                                                navigator.to("home.fish.detail", item.name, item);
                                                this.changeRoute("home.fish");
                                                break;

                                            case 'Птицы':
                                                navigator.to("home.birds.detail", item.name, item);
                                                this.changeRoute("home.birds");
                                                break;
                                        }
                                    }}
                                    onLongPress={() => {
                                        switch (item.category) {
                                            case 'Собаки':
                                                navigator.to("home.dogs.detail", item.name, item);
                                                this.changeRoute("home.dogs");
                                                break;

                                            case 'Кошки':
                                                navigator.to("home.cats.detail", item.name, item);
                                                this.changeRoute("home.cats");
                                                break;

                                            case 'Грызуны':
                                                navigator.to("home.rodents.detail", item.name, item);
                                                this.changeRoute("home.rodents");
                                                break;

                                            case 'Рыбки':
                                                navigator.to("home.fish.detail", item.name, item);
                                                this.changeRoute("home.fish");
                                                break;

                                            case 'Птицы':
                                                navigator.to("home.birds.detail", item.name, item);
                                                this.changeRoute("home.birds");
                                                break;
                                        }
                                     }}>
                                    <Card.Body style={styles.item}>
                                        <View style={styles.itemLeft}>
                                            <Avatar size={50} borderRadius={50} image={<Image source={{ uri: "http://nikitayt.bget.ru"+item.pic }} />} />
                                            <View style={styles.text}>
                                                <Text style={styles.title}>{item.name}</Text>
                                                <Text>{item.category}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.itemRight}>
                                            <Icon name="keyboard-arrow-right" color="rgba(0,0,0,.87)" size={22} />
                                        </View>
                                    </Card.Body>
                                </TouchableNativeFeedback>
                            </Card>
                        </Swipeout>
                    ))}
                </View>;
            }
        }

        return (
            <ScrollView>
                {content}
            </ScrollView>
        );
    }
}

const styles = {
    spinnerContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    contFirst: {
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 4,
        marginTop: 8,
    },
    cont: {
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 4,
        marginTop: 4,
    },
    item: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 13,
        paddingBottom: 13,
        paddingLeft: 16,
        paddingRight: 16,
        flexDirection: 'row'
    },
    itemLeft: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row'
    },
    itemRight: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row'
    },
    text: {
        paddingLeft: 14
    },
    title: {
        fontWeight: 'bold',
        color: 'rgba(0,0,0,.87)'
    }
};