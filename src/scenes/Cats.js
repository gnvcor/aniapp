import React, { Component, PropTypes } from 'react';
import {
    ScrollView,
    TouchableNativeFeedback,
    View,
    Image,
    Text
} from 'react-native';
import { Avatar, Subheader, COLOR, Card, Icon } from 'react-native-material-design';

import api from '../network/json/cat.json';

export default class Cats extends Component {

    constructor() {
        super();
        this.data = api;
    }

    static contextTypes = {
        navigator: PropTypes.object.isRequired
    };

    render() {
        var data = JSON.parse(JSON.stringify(this.data));
        var array = [];
        const { navigator } = this.context;

        for (var i in data) {
            array.push(data[i]);
        }

        return (
            <ScrollView>
                {array.map((item, i) => (
                    <Card key={i} style={i == 0 ? styles.contFirst : styles.cont}>
                        <TouchableNativeFeedback
                            background={TouchableNativeFeedback.Ripple("rgba(153,153,153,.4)")}
                            onPress={() => { navigator.forward('', item.name, item) }}
                            onLongPress={() => { navigator.forward('', item.name, item) }}>
                            <Card.Body style={styles.item}>
                                <View style={styles.itemLeft}>
                                    <Avatar size={50} borderRadius={50} image={<Image source={{ uri: "http://nikitayt.bget.ru"+item.pic }} />} />
                                    <View style={styles.text}>
                                        <Text style={styles.title}>{item.name}</Text>
                                        <Text>{item.category}</Text>
                                    </View>
                                </View>
                                <View style={styles.itemRight}>
                                    <Icon name="keyboard-arrow-right" color="rgba(0,0,0,.87)" size={22} />
                                </View>
                            </Card.Body>
                        </TouchableNativeFeedback>
                    </Card>
                ))}
            </ScrollView>
        );
    }
}

const styles = {
    contFirst: {
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 8,
        marginTop: 8,
    },
    cont: {
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 8,
        marginTop: 0,
    },
    item: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 13,
        paddingBottom: 13,
        paddingLeft: 16,
        paddingRight: 16,
        flexDirection: 'row'
    },
    itemLeft: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row'
    },
    itemRight: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row'
    },
    text: {
        paddingLeft: 14
    },
    title: {
        fontWeight: 'bold',
        color: 'rgba(0,0,0,.87)'
    }
};