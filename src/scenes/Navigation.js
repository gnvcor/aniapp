import React, { Component, PropTypes } from 'react';
import {
    View,
    Text,
    Image,
    StatusBar,
    ScrollView,
    AsyncStorage
} from 'react-native';

import { Avatar, Drawer, Divider, COLOR, TYPO } from 'react-native-material-design';
import AppStore from '../stores/AppStore';

import AppRoutes from '../actions/AppRoutes';
import AppUser from '../actions/AppUser';

import Firebase from 'firebase';

export default class Navigation extends Component {

    static contextTypes = {
        drawer: PropTypes.object.isRequired,
        navigator: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            route: 'home',
            theme: AppStore.getState().theme,
            user: AppStore.getState().user,
            login: false
        }
    }

    changeRoute = (route) => {
        AppRoutes.updateRoutes(route);
    };

    changeScene = (path, name) => {
        const { drawer, navigator } = this.context;

        this.setState({
            route: path
        });
        navigator.to(path, name);
        drawer.closeDrawer();
    };

    componentDidMount = () => {
        AppStore.listen(this.handleAppStore);
    };

    componentWillUnmount() {
        AppStore.unlisten(this.handleAppStore);
    };

    handleAppStore = (store) => {
        this.setState({
            theme: store.theme,
            route: store.route,
            user: store.user
        });
    };

    logout(){
        const { drawer, navigator } = this.context;

        AsyncStorage.removeItem('@Storage:user').then(() => {
            Firebase.auth().signOut();
            navigator.to("auth");
            drawer.closeDrawer();
            AppUser.updateUser(null);
        });

    }

    render() {
        const { route, theme, user } = this.state;

        var personal = <Divider style={styles.divider}/>;
        var customDrawer = <Divider style={styles.divider}/>;
        var auth = <Drawer.Section
                title="Настройки"
                items={[{
                    icon: 'verified-user',
                    value: 'Авторизация',
                    active: route === 'auth',
                    onPress: () => this.changeScene('auth'),
                    onLongPress: () => this.changeScene('auth')
                },{
                    icon: 'invert-colors',
                    value: 'Изменить тему',
                    active: route === 'themes',
                    onPress: () => this.changeScene('themes'),
                    onLongPress: () => this.changeScene('themes')
                },{
                    icon: 'info',
                    value: 'О приложении',
                    active: route === 'about',
                    onPress: () => this.changeScene('about'),
                    onLongPress: () => this.changeScene('about')
                }]}
        />;

        if (user != null) {
            personal = <Drawer.Section
                title="Личный кабинет"
                items={[{
                    icon: 'star',
                    value: 'Мои животные',
                    active: route === 'personal',
                    onPress: () => this.changeScene('personal'),
                    onLongPress: () => this.changeScene('personal')
                }]}
            />;

            customDrawer = <Divider style={{ marginTop: 8 }}/>;

            auth = <Drawer.Section
                title="Настройки"
                items={[{
                    icon: 'exit-to-app',
                    value: 'Выйти',
                    active: route === 'auth',
                    onPress: () => {this.logout(); this.changeRoute("auth");},
                    onLongPress: () => {this.logout(); this.changeRoute("auth");}
                },{
                    icon: 'invert-colors',
                    value: 'Изменить тему',
                    active: route === 'themes',
                    onPress: () => this.changeScene('themes'),
                    onLongPress: () => this.changeScene('themes')
                },{
                    icon: 'info',
                    value: 'О приложении',
                    active: route === 'about',
                    onPress: () => this.changeScene('about'),
                    onLongPress: () => this.changeScene('about')
                }]}
            />;
        }

        return (
            <ScrollView>
                <StatusBar
                    backgroundColor={COLOR[`${theme == undefined ? "paperTeal" : theme}700`].color}
                />
                <Drawer theme='light'>
                    <Drawer.Section
                        items={[{
                            icon: 'home',
                            value: 'Главная',
                            active: !route || route === 'home',
                            onPress: () => {this.changeScene('home'); this.changeRoute("home");},
                            onLongPress: () => {this.changeScene('home'); this.changeRoute("home");}
                        }]}
                    />

                    {personal}
                    {customDrawer}

                    <Drawer.Section
                        title="Животные"
                        items={[{
                            icon: 'label',
                            image: 'http://nikitayt.bget.ru/menuImages/dog.jpg',
                            value: 'Собаки',
                            active: route === 'home.dogs',
                            onPress: () => {this.changeScene('home.dogs');  this.changeRoute("home.dogs");},
                            onLongPress: () => {this.changeScene('home.dogs');  this.changeRoute("home.dogs");}
                        }, {
                            icon: 'label',
                            image: 'http://nikitayt.bget.ru/menuImages/cat.jpg',
                            value: 'Кошки',
                            active: route === 'home.cats',
                            onPress: () => {this.changeScene('home.cats'); this.changeRoute("home.cats");},
                            onLongPress: () => {this.changeScene('home.cats'); this.changeRoute("home.cats");}
                        }, {
                            icon: 'label',
                            value: 'Грызуны',
                            image: 'http://nikitayt.bget.ru/menuImages/rabbit.jpg',
                            active: route === 'home.rodents',
                            onPress: () => {this.changeScene('home.rodents'); this.changeRoute("home.rodents");},
                            onLongPress: () => {this.changeScene('home.rodents'); this.changeRoute("home.rodents");},
                        }, {
                            icon: 'label',
                            value: 'Рыбки',
                            image: 'http://nikitayt.bget.ru/menuImages/fish.jpg',
                            active: route === 'home.fish',
                            onPress: () => {this.changeScene('home.fish'); this.changeRoute("home.fish");},
                            onLongPress: () => {this.changeScene('home.fish'); this.changeRoute("home.fish");}
                        }, {
                            icon: 'label',
                            value: 'Птицы',
                            image: 'http://nikitayt.bget.ru/menuImages/birds.jpg',
                            active: route === 'home.birds',
                            onPress: () => {this.changeScene('home.birds'); this.changeRoute("home.birds");},
                            onLongPress: () => this.changeScene('home.birds')
                        }/*, {
                            icon: 'label',
                            value: 'Экзотические животные',
                            image: 'http://nikitayt.bget.ru/menuImages/exotic2.jpg',
                            active: route === 'home.exotic-animals',
                            onPress: () => this.changeScene('home.exotic-animals'),
                            onLongPress: () => this.changeScene('home.exotic-animals')
                        }*/]}
                    />
                    <Divider style={{ marginTop: 8 }}/>
                    {auth}
                </Drawer>
            </ScrollView>
        );
    }
}

const styles = {
    header: {
        paddingTop: 16
    },
    text: {
        marginTop: 20
    },
    divider: {
        height: 0
    }
};