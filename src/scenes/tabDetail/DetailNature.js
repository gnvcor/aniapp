import React, { Component } from 'react';
import {
    Text,
    ScrollView,
    Image
} from 'react-native';

export default class DetailNature extends Component {
    render() {
        return (
            <ScrollView style={styles.cont}>
                <Text style={styles.infoText}>{this.props.data.natureText}</Text>
            </ScrollView>
        );
    }
}

const styles = {
    cont: {
        padding: 16
    },
    infoText: {
        lineHeight: 30,
        marginBottom: 20
    }
};