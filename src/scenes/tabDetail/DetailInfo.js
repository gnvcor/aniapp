import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    TouchableHighlight
} from 'react-native';

import ParallaxScrollView from 'react-native-parallax-scroll-view';

import Icon from 'react-native-vector-icons/MaterialIcons';

import AppStore from '../../stores/AppStore';

import { Button, COLOR } from 'react-native-material-design';

import Firebase from 'firebase';

import GiftedSpinner from 'react-native-gifted-spinner';

import PushNotification from 'react-native-push-notification';

export default class DetailInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            theme: AppStore.getState().theme,
            user: AppStore.getState().user,
            active: false,
            loaded: true,
            defaultButton: false
        };

        if (this.state.user != null) {
            this.itemsRef = this.getRef().child(this.state.user).child("animal");
        }
    }

    push(name) {
        PushNotification.localNotification({
            autoCancel: true,
            largeIcon: "ic_launcher",
            smallIcon: 'ic_grade_white_48dp',
            title: "Уведомление AniApp",
            message: name + " добавлен в личный кабинет!",
            playSound: true
        });
    }

    getRef() {
        return Firebase.database().ref();
    }

    _addItem() {
        this.push(this.props.data.name);
        this.setState({
            active: true
        });
        this.itemsRef.push({
            name: this.props.data.name,
            uri: this.props.data.pic,
            category: this.props.data.category,
            numberParams: {
                pound: this.props.data.numberParams.pound,
                height: this.props.data.numberParams.height,
                age: this.props.data.numberParams.age
            },
            infoText: this.props.data.infoText,
            natureText: this.props.data.natureText,
            factText: this.props.data.factText,
            diseaseText: this.props.data.diseaseText,
            careText: this.props.data.careText
        })
    }

    searchArray(array, value) {

        for (var i = 0; i < array.length; i++) {
            if (array[i].name == value) return i;
        }

        return -1;
    }

    listenForItems(itemsRef) {
        itemsRef.on('value', (snap) => {

            var items = [];
            snap.forEach((child) => {
                items.push({
                    name: child.val().name,
                    uri: child.val().uri,
                    category: child.val().category
                });
            });

            if (this.searchArray(items, this.props.data.name) != -1) {
                this.setState({
                    active: true,
                    loaded: false
                });
            } else {
                this.setState({
                    defaultButton: true
                });
            }

        });
    }

    componentDidMount = () => {
        if (this.state.user != null) {
            AppStore.listen(this.handleAppStore);
            this.listenForItems(this.itemsRef);
        }
    };

    componentWillUnmount() {
        if (this.state.user != null) {
            AppStore.unlisten(this.handleAppStore);
        }
    };

    handleAppStore = (store) => {
        this.setState({
            theme: store.theme,
            user: store.user
        });
    };

    render() {
        const { theme, user } = this.state;
        var button = <View></View>;

        if (user != null) {
            if (this.state.loaded == true) {
                button = <GiftedSpinner style={{position: 'absolute', right: 37, zIndex: 10, bottom: 37}}/>;

                if (this.state.defaultButton == true) {
                    button = <TouchableHighlight style={{position: 'absolute', right: 20, zIndex: 10, bottom: 20, borderRadius: 60}} onPress={this.state.active != true ? this._addItem.bind(this) : () => {return false}}>
                        <View style={[{backgroundColor: COLOR[`${theme}500`].color, flex: 1, alignItems: 'center', justifyContent: 'center'}, styles.iconParam]}>
                            <Icon name={this.state.active == true ? "star" : "star-border"} size={30} color="#fff" />
                        </View>
                    </TouchableHighlight>;
                }
            } else {
                button = <TouchableHighlight style={{position: 'absolute', right: 20, zIndex: 10, bottom: 20, borderRadius: 60}} onPress={this.state.active != true ? this._addItem.bind(this) : () => {return false}}>
                    <View style={[{backgroundColor: COLOR[`${theme}500`].color, flex: 1, alignItems: 'center', justifyContent: 'center'}, styles.iconParam]}>
                        <Icon name={this.state.active == true ? "star" : "star-border"} size={30} color="#fff" />
                    </View>
                </TouchableHighlight>;
            }
        }

        return (
            <ParallaxScrollView
                backgroundColor={COLOR[`${theme}500`].color}
                contentBackgroundColor="#eeeeee"
                parallaxHeaderHeight={250}
                renderForeground={() => (
                <View>
                    <Image source={{ uri: "http://nikitayt.bget.ru"+this.props.data.pic, width: window.width, height: 250 }}/>
                    {button}
                </View>
                )}>
                <View style={styles.cont}>
                    <Text style={styles.name}>{this.props.data.name}</Text>
                    <Text style={styles.title}>{this.props.data.category}</Text>
                    <View style={styles.contIcon}>
                        <View style={[styles.item]}>
                            <View style={[{backgroundColor: COLOR[`${theme}500`].color}, styles.iconParam]}>
                                <Text style={styles.countText}>
                                    {this.props.data.numberParams.pound != '-' ? 'до ' : ''}{this.props.data.numberParams.pound}
                                </Text>
                            </View>
                            <Text style={styles.countTitle}>
                                Вес, кг
                            </Text>
                        </View>
                        <View style={[styles.item]}>
                            <View style={[{backgroundColor: COLOR[`${theme}500`].color}, styles.iconParam]}>
                                <Text style={styles.countText}>
                                    {this.props.data.numberParams.height != '-' ? 'до ' : ''}{this.props.data.numberParams.height}
                                </Text>
                            </View>
                            <Text style={styles.countTitle}>
                                {this.props.data.category == 'Грызуны' || this.props.data.category == 'Рыбки' ? 'Длина' : 'Рост' }, см
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <View style={[{backgroundColor: COLOR[`${theme}500`].color}, styles.iconParam]}>
                                <Text style={styles.countText}>
                                    {this.props.data.numberParams.age != '-' ? 'до ' : ''}{this.props.data.numberParams.age}
                                </Text>
                            </View>
                            <Text style={styles.countTitle}>
                                Возраст, лет
                            </Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.infoText}>
                            {this.props.data.infoText}
                        </Text>
                    </View>
                </View>
            </ParallaxScrollView>
        );
    }
}

const styles = {
    cont: {
        padding: 16
    },
    parallaxCont: {
        flex: 1
    },
    name: {
        fontSize: 22,
        color: 'rgba(0,0,0,.87)',
        fontWeight: 'bold'
    },
    title: {
        fontSize: 15,
        color: 'rgba(153,153,153,.8)'
    },
    contIcon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: 20
    },
    iconParam: {
        height: 60,
        width: 60,
        borderRadius: 60
    },
    countText: {
        height: 60,
        width: 60,
        textAlign: 'center',
        fontSize: 15,
        color: 'white',
        marginTop: 18
    },
    countTitle: {
        textAlign: 'center',
        marginTop: 5,
        width: 100,
        fontSize: 12
    },
    item: {
        width: 100,
        flex: 1,
        alignItems: 'center'
    },
    infoText: {
        marginTop: 20,
        lineHeight: 30
    },
    buttonAdd: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        zIndex: 5
    }
};